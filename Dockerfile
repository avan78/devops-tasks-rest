FROM python:3.9.2

COPY requirements.txt /srv/petvet/requirements.txt

RUN pip3 install -r /srv/petvet/requirements.txt

COPY docker-entrypoint.sh /srv/petvet/docker-entrypoint.sh

ENTRYPOINT [ "migrate", "/docker-entrypoint.sh" ]

COPY clinic/ /srv/petvet/

CMD [ "gunicorn", "/clinic/wsgi.py" ]